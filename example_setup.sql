/*
ALIST - a PHP and JavaScript library for creating sortable, filterable lists
from MySQL tables.

Copyright Adam D. Hincks, S.J., 2015.

This file is part of ALIST.

ALIST is free software: you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software 
Foundation, either version 3 of the License, or (at your option) any later 
version.

ALIST is distributed in the hope that it will be useful, but WITHOUT ANY 
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with 
ALIST.  If not, see <http://www.gnu.org/licenses/>.
*/

-- Create tables for use with the alist example and populate them with initial
-- data.

USE test;

DROP TABLE IF EXISTS alist_creator;
CREATE TABLE alist_creator (id INT NOT NULL PRIMARY KEY UNIQUE AUTO_INCREMENT, \
                            name VARCHAR(128) NOT NULL, \
                            notes TEXT);

DROP TABLE IF EXISTS alist_dept;
CREATE TABLE alist_dept (id INT NOT NULL PRIMARY KEY UNIQUE AUTO_INCREMENT, \
                         name VARCHAR(128) NOT NULL,
                         notes TEXT);

DROP TABLE IF EXISTS alist_subdept;
CREATE TABLE alist_subdept (id INT NOT NULL PRIMARY KEY UNIQUE AUTO_INCREMENT, \
                            dept_id INT NOT NULL,
                            name VARCHAR(128) NOT NULL,
                            notes TEXT);

DROP TABLE IF EXISTS alist_thing;
CREATE TABLE alist_thing (id INT NOT NULL PRIMARY KEY UNIQUE AUTO_INCREMENT, \
                          name VARCHAR(128) NOT NULL, \
                          height FLOAT, \
                          creator_id INT NOT NULL, \
                          dept_id INT NOT NULL, \
                          subdept_id INT, \
                          date_started DATETIME NOT NULL,
                          in_development ENUM('Y', 'N') NOT NULL, \
                          notes TEXT, \
                          FOREIGN KEY (creator_id) \
                            REFERENCES alist_creator(id), \
                          FOREIGN KEY (dept_id) \
                            REFERENCES alist_dept(id), \
                          FOREIGN KEY (subdept_id) \
                            REFERENCES alist_subdept(id));

INSERT INTO alist_creator(name, notes) VALUES \
         ("Willy Wonka", "He's a bit weird.");
INSERT INTO alist_creator(name, notes) VALUES \
         ("Big Bird", NULL);
INSERT INTO alist_creator(name, notes) VALUES \
         ("Lowly the Worm", "Watch out for the pickles!");
INSERT INTO alist_creator(name, notes) VALUES \
         ("Julius Caesar", NULL);

INSERT INTO alist_dept(name, notes) VALUES ("research", NULL);
INSERT INTO alist_dept(name, notes) VALUES ("marketing", "Let's make profit!");
INSERT INTO alist_dept(name, notes) VALUES ("funorama", "It's awesome!!");

INSERT INTO alist_subdept(dept_id, name, notes) VALUES \
         (1, "foodstuffs", "Mmm, mmm!");
INSERT INTO alist_subdept(dept_id, name, notes) VALUES \
         (1, "transportation", NULL);
INSERT INTO alist_subdept(dept_id, name, notes) VALUES \
         (1, "warfare", NULL);
INSERT INTO alist_subdept(dept_id, name, notes) VALUES \
         (2, "national", NULL);
INSERT INTO alist_subdept(dept_id, name, notes) VALUES \
         (2, "international", NULL);

INSERT INTO alist_thing (name, creator_id, dept_id, subdept_id, date_started, \
                         height, in_development, notes) VALUES \
       ("pickle", 3, 1, 1, "1972/04/09 12:00", 12.2, "N", NULL);
INSERT INTO alist_thing (name, creator_id, dept_id, subdept_id, date_started, \
                         height, in_development, notes) VALUES \
       ("glass elevator", 1, 1, 2, "1979/09/12 13:40", 2.332, "Y", "Weeeeeee!");
INSERT INTO alist_thing (name, creator_id, dept_id, subdept_id, date_started, \
                         height, in_development, notes) VALUES \
       ("silly putty", 1, 3, NULL, "1973/02/23 03:23", 123.33223, "N", NULL);
INSERT INTO alist_thing (name, creator_id, dept_id, subdept_id, date_started, \
                         height, in_development, notes) VALUES \
       ("short sword", 4, 1, 3, "1905/01/02 11:53", NULL, "N", \
        "Is the date right?");
INSERT INTO alist_thing (name, creator_id, dept_id, subdept_id, date_started, \
                         height, in_development, notes) VALUES \
       ("the letter B", 2, 2, 1, "1999/06/01 15:01", 1, "Y", "B is for bird.");
INSERT INTO alist_thing (name, creator_id, dept_id, subdept_id, date_started, \
                         height, in_development, notes) VALUES \
       ("apple pie", 3, 1, 1, "1971/05/13 01:19", 22.3, "N", NULL);
INSERT INTO alist_thing (name, creator_id, dept_id, subdept_id, date_started, \
                         height, in_development, notes) VALUES \
       ("memoirs", 4, 3, NULL, "1902/05/23 05:26", 1e-5, "N", NULL);
INSERT INTO alist_thing (name, creator_id, dept_id, subdept_id, date_started, \
                         height, in_development, notes) VALUES \
       ("bubble gum", 1, 1, 1, NOW(), NULL, "Y", \
        "The flavour is still running out.");

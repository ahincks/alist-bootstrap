/*
ALIST - a PHP and JavaScript library for creating sortable, filterable lists
from MySQL tables.

Copyright Adam D. Hincks, S.J., 2015.

This file is part of ALIST.

ALIST is free software: you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software 
Foundation, either version 3 of the License, or (at your option) any later 
version.

ALIST is distributed in the hope that it will be useful, but WITHOUT ANY 
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with 
ALIST.  If not, see <http://www.gnu.org/licenses/>.
*/

function alist_get_query(name) {
  name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
  var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
                  results = regex.exec(location.search);
  return results == null ? "" :
                           decodeURIComponent(results[1].replace(/\+/g, " "));
}

function alist_submit_query(key, value, ignore) {
  q_str = location.search;
  q_str = alist_update_query(q_str, key, value, ignore);
  location.search = q_str;
}

function alist_update_query(q, key, value, ignore) {
  var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
  if (value == ignore)
    value = "";

  separator = q.indexOf('?') !== -1 ? "&" : "?";
  if (q.match(re)) {
    if (value.length)
      q = q.replace(re, '$1' + key + "=" + value + '$2');
    else
      q = q.replace(re, '$1$2');
  }
  else {
    if (value.length)
      q = q + separator + key + "=" + value;
    else
      q = q;
  }

  // Clean up orphaned &'s.
  if (q.slice(-1) == '&')
    q = q.slice(0, q.length - 1);

  // Clean up any doubled &'s.
  while ((i = q.indexOf('&&')) > -1)
    q = q.slice(0, i) + 
        q.slice(i + 1);

  return q;
}

function alist_order(table, field) {
  if (!(curr_field = alist_get_query(table + "_nav_order_by")))
    curr_field = field;
  if (!(curr_dir = alist_get_query(table + "_nav_order_dir")))
    curr_dir = "ASC";

  q_str = location.search;
  q_str = alist_update_query(q_str, table + "_nav_order_by", field, "");
  if (field == curr_field && curr_dir == "ASC")
    q_str = alist_update_query(q_str, table + "_nav_order_dir", "DESC", "");
  else
    q_str = alist_update_query(q_str, table + "_nav_order_dir", "ASC", "");
  location.search = q_str;
}

function alist_show_combo(controller, container_id) {
  suffix = controller.value;
  div_list = document.getElementById(container_id).children;
  for (i = 0; i < div_list.length; i++) {
    if (div_list[i].id == container_id + "_" + suffix)
      div_list[i].style.display = "block";
    else
      div_list[i].style.display = "none";
  }
}

function alist_new(table) {
  q_str = location.search;
  q_str = alist_update_query(q_str, table + "_new", "1", "");
  location.search = q_str;
}

function alist_edit(table, id) {
  q_str = location.search;
  q_str = alist_update_query(q_str, table + "_edit", id, "");
  location.search = q_str;
}

function alist_submit(table) {
  href = location.href;
  href = alist_update_query(href, table + "_edit", "", "");
  href = alist_update_query(href, table + "_new", "", "");
  form = document.getElementById("alist_" + table);
  form.action = href;
  form.submit();
}

function alist_cancel(table) {
  q_str = location.search;
  q_str = alist_update_query(q_str, table + "_edit", "", "");
  q_str = alist_update_query(q_str, table + "_new", "", "");
  location.search = q_str;
}

function alist_go_page(table, n) {
  q_str = location.search;
  q_str = alist_update_query(q_str, table + "_nav_first_row", n.toString(), "");
  q_str = alist_update_query(q_str, table + "_new", "", "");
  q_str = alist_update_query(q_str, table + "_edit", "", "");
  location.search = q_str;
}

function alist_check_enter(e, f, arg) {
  var key;

  if (window.event)
    key = window.event.keyCode;
  else
    key = e.keycode || e.which;
  if (key == "13") {
    a = Array.prototype.slice.call(arguments, 2);
    f(a);
    return false;
  }

  return true;
}

function alist_nav_perpage(table) {
  n = parseInt(document.getElementById(table + "_nav_perpage").value);
  if (isNaN(n) || n < 1) {
    window.alert("Number of rows per page must be a positive integer.");
    return;
  }

  q_str = location.search;
  q_str = alist_update_query(q_str, table + "_nav_perpage", n.toString(), "");
  q_str = alist_update_query(q_str, table + "_new", "", "");
  q_str = alist_update_query(q_str, table + "_edit", "", "");
  location.search = q_str;
}

function alist_n_filter(table, n) {
  q_str = location.search;
  q_str = alist_update_query(q_str, table + "_n_filt", n.toString(), "");
  q_str = alist_update_query(q_str, table + "_new", "", "");
  q_str = alist_update_query(q_str, table + "_edit", "", "");
  q_str = alist_update_query(q_str, table + "_nav_first_row", "", "");
  location.search = q_str;
}

function alist_apply_filter(ref, filt, filt_cancel) {
  q_str = location.search;
  q_str = alist_update_query(q_str, filt, ref.value);
  if (filt_cancel) {
    for (ii = 0; ii < filt_cancel.length; ii++) {
      q_str = alist_update_query(q_str, filt_cancel[ii], "");
    }
  }
  location.search = q_str;
}

function alist_kill_filter(table, n, field_list) {
  q_str = location.search;
  for (ii = 0; ii < n; ii++) {
    for (j = 0; j < field_list.length; j++)
      q_str = alist_update_query(q_str, table + "_" + field_list[j] + "_filt_" +
                                ii, "");
  }
  q_str = alist_update_query(q_str, table + "_n_filt", "");
  location.search = q_str;
}

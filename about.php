<!DOCTYPE html>
<html lang="en">

<head>
  <title>ALIST using Bootstrap</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://select2.github.io/select2/select2-3.5.2/select2.css" type="text/css">
  <link rel="stylesheet" href="https://fk.github.io/select2-bootstrap-css/css/select2-bootstrap.css" type="text/css">
  <script type="text/javascript" src="alist.js"></script>
  <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
  <script type="text/javascript" src="https://select2.github.io/select2/select2-3.4.2/select2.js"></script>
 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
 <style>
   .prototype {
     margin-left: 6em;
     text-indent: -6em;
   }
 </style>
</head>

<body>

<div class="container">

<div class="page-header">
  <h1>
    <tt>ALIST</tt>
    <small>Flexible display of database tables rendered with bootstrap.</small>
  </h1>
</div>

<p><tt>ALIST</tt> is a PHP and JavaScript package that generates sortable,
searchable, editable tables of data on your webpage from a database. It uses
<a href="http://getbootstrap.com">bootstrap CSS styles</a>. <tt>ALIST</tt> is
written and maintained by <a href="http://adh-sj.info">Adam Hincks
S.J.</a> and is released under the <a
href="http://www.gnu.org/licenses/gpl-3.0.en.html">GPL</a>.

<p>The most current code can be downloaded from
<a href="https://gitlab.com/ahincks/alist-bootstrap">Gitlab</a>, and you can
also download a release here (which is recommended for most use):</p>
<ul>
  <li><a href="releases/alist-bootstrap-1.0.0.zip">alist-bootstrap-1.0.0.zip</a>
  (3 August 2015)</li>
</ul>

<p><tt>ALIST</tt> requires the following PHP/JavaScript libraries:
<a href="https://jquery.com/">JQuery</a>,
<a href="http://getbootstrap.com/">Bootstrap</a>, 
<a href="http://select2.github.io/select2/">Select2</a>,
<a href="https://fk.github.io/select2-bootstrap-css/">Select2 Bootstrap 3
  CSS</a>.</p>

<h2>Example</h2>

<script>
  function example_action(id) {
    alert("The ID is " + id + ".");
  }
  
  <? // For on-line demo, disable changing the database by overriding the
  //following function.
  if ($_SERVER["SERVER_NAME"] == "http://alist.adh-sj.info" ||
      $_SERVER["SERVER_NAME"] == "https://alist.adh-sj.info") { ?>
    function alist_submit() {
      alert("In this demo, editing of the database is disabled.");
    }
  <? } ?>
</script>
<?php
  include("alist.php");

  // PHP will complain unless you set the timezone.
  date_default_timezone_set("America/Vancouver");

  $db = new mysqli("localhost", "test", "password", "test");

  $a = new alist($db);
  $a->set_table_and_pkey("alist_thing", "id");
  $a->set_default_perpage(5);
  $a->allow_edit();
  $a->allow_new();

  $name = new col_str("name", "Name");
  $name->action = new action("example_action", "id");
  $a->add_column($name);
  $a->add_column(new col_numeric("height", "Height (mm)", "%.2f"));
  $a->add_column(new col_combo("creator_id", "Creator", "alist_creator",
                               "name"));
  $a->add_column(new col_combo("dept_id", "Department", "alist_dept", "name"));
  $a->add_column(new col_combo("subdept_id", "Subdepartment", "alist_subdept",
                               "name", "id", "dept_id", "dept_id"));
  $a->add_column(new col_datetime("date_started", "Start&nbsp;Date",
                                  "%d %b. %Y"));
  $a->add_column(new col_enum("in_development", "In&nbsp;Devel.",
                              array("Y" => "Yessir", "N" => "Nope")));
  $a->add_column(new col_str("notes", "Notes", 20));

  $a->make();
?>

<h3>Database Schema</h3>

<p>Here is the schema of the database table displayed above. (It is in MySQL,
but you can use any database software that talks to
<a href="http://php.net/manual/it/book.mysqli.php">mysqli</a> or 
<a href="http://php.net/manual/en/book.pdo.php">PDO</a>.)

<pre>
mysql> describe alist_thing;
+----------------+---------------+------+-----+---------+----------------+
| Field          | Type          | Null | Key | Default | Extra          |
+----------------+---------------+------+-----+---------+----------------+
| id             | int(11)       | NO   | PRI | NULL    | auto_increment |
| name           | varchar(128)  | NO   |     | NULL    |                |
| height         | float         | YES  |     | NULL    |                |
| creator_id     | int(11)       | NO   | MUL | NULL    |                |
| dept_id        | int(11)       | NO   | MUL | NULL    |                |
| subdept_id     | int(11)       | YES  | MUL | NULL    |                |
| date_started   | datetime      | NO   |     | NULL    |                |
| in_development | enum('Y','N') | NO   |     | NULL    |                |
| notes          | text          | YES  |     | NULL    |                |
+----------------+---------------+------+-----+---------+----------------+
9 rows in set (0.00 sec)
</pre>

<p>Note that three of the columns (<code>creator_id</code>,
<code>dept_id</code> and <code>subdept_id</code>) are foreign keys. One of the
charms of <tt>alist</tt> is that it will insert the value from the linked table.
Here are the relevant tables:</p>

<pre>
mysql> describe alist_creator;
+-------+--------------+------+-----+---------+----------------+
| Field | Type         | Null | Key | Default | Extra          |
+-------+--------------+------+-----+---------+----------------+
| id    | int(11)      | NO   | PRI | NULL    | auto_increment |
| name  | varchar(128) | NO   |     | NULL    |                |
| notes | text         | YES  |     | NULL    |                |
+-------+--------------+------+-----+---------+----------------+
3 rows in set (0.00 sec)

mysql> describe alist_dept;
+-------+--------------+------+-----+---------+----------------+
| Field | Type         | Null | Key | Default | Extra          |
+-------+--------------+------+-----+---------+----------------+
| id    | int(11)      | NO   | PRI | NULL    | auto_increment |
| name  | varchar(128) | NO   |     | NULL    |                |
| notes | text         | YES  |     | NULL    |                |
+-------+--------------+------+-----+---------+----------------+
3 rows in set (0.00 sec)

mysql> describe alist_subdept;
+---------+--------------+------+-----+---------+----------------+
| Field   | Type         | Null | Key | Default | Extra          |
+---------+--------------+------+-----+---------+----------------+
| id      | int(11)      | NO   | PRI | NULL    | auto_increment |
| dept_id | int(11)      | NO   |     | NULL    |                |
| name    | varchar(128) | NO   |     | NULL    |                |
| notes   | text         | YES  |     | NULL    |                |
+---------+--------------+------+-----+---------+----------------+
4 rows in set (0.01 sec)
</pre>

<h3>PHP Code</h3>

<p>Here is a walk-through the PHP code used to generate the above. First, we
set the time-zone (so that PHP doesn't complain when date methods are
used), open the connection to the database and instantiate a new
<code>alist</code> object:</p>
<?
  function hilite($str) {
    $s = highlight_string("<?" . $str . "?>", true);
    $i = strpos($s, "\n");
    $s = substr($s, $i + 1);
    $i = strpos($s, "\n");
    $s = substr($s, 0, $i + 1);
    $s = str_replace("&lt;?", "", $s);
    $s = str_replace("?&gt;", "", $s);
    $s = str_replace(",&nbsp;", ", ", $s);
    echo $s;
  }
?>

<pre><?hilite(
'date_default_timezone_set("America/Vancouver");
$db = new mysqli("localhost", "test", "password", "test");
$a = new alist($db);'
);?></pre>

<p>Next, we take care of some global settings. The first line is required
because it tells <tt>alist</tt> which database table to render. The other lines
are optional and self-explanatory.

<pre><?hilite(
'$a->set_table_and_pkey("alist_thing", "id");
$a->set_default_perpage(5);
$a->allow_edit();
$a->allow_new();'
);?></pre>

<p>Now we can start adding columns to the table. The first column will be
<tt>name</tt>, which is a string. We instantiate a new <code>col_str</code>.
Then, as an optional step, we add an action to it. The arguments of the action 
are (1) a JavaScript function 
that will be fired on the <code>onclick</code> event and (2) the database column
of the value that should be passed to (1) when the row is clicked. Hence, in our
example the id of the table row is passed to the function
<code>example_action</code> when the name is clicked. Finally, we use
<code>add_column</code> to add the column to our table.</p>

<pre><?hilite(
'$name = new col_str("name", "Name");
$name->action = new action("example_action", "id");
$a->add_column($name);'
);?></pre>

<p>We now add the rest of the columns; note that unlike the &lsquo;name&rsquo;
column we do not actions to them. (The arguments for the column constructors <a
href="#col_reference">are explained below</a>.)</p>

<pre><?hilite(
'$a->add_column(new col_combo("creator_id", "Creator", "alist_creator",
                              "name"));
$a->add_column(new col_combo("dept_id", "Department", "alist_dept", "name"));
$a->add_column(new col_combo("subdept_id", "Subdepartment", "alist_subdept",
                             "name", "id", "dept_id", "dept_id"));
$a->add_column(new col_datetime("date_started", "Start&nbsp;Date",
                                "%d %b. %Y"));
$a->add_column(new col_enum("in_development", "In&nbsp;Devel.",
                            array("Y" => "Yessir", "N" => "Nope")));
$a->add_column(new col_str("notes", "Notes", 20));'
);?></pre>

<p>Finally, with a single command we render the table:</p>

<pre><?hilite('$a->make();');?></pre>

<p><b>N.B.:</b> You should ensure that the <code>make()</code> method occurs 
within
a div with the <code>container</code> or <code>container-fluid</code> class, <a
href="http://getbootstrap.com/css/#overview-container">as is normal in the
bootstrap framework</a>.</p>

<br>
<h2>Reference</h2>

<h3>Global Options</h3>

<p>A number of options can be set via methods of an <code>alist</code> object.</p>
<ul>
  <li><b><?hilite('set_table_and_pkey($table, $pkey = "id")');?></b> &mdash; set
    the table and specify the name of its primary key
  <li><b><?hilite('set_null_val($null_val)');?></b> &mdash; text to display if
    a database value is null (default = <tt>N/A</tt>)</li>
  <li><b><?hilite('set_default_perpage($n)');?></b> &mdash; number of rows to
    display per page when first loaded (default = 25)</li>
  <li><b><?hilite('allow_edit()');?></b> &mdash; allow editing of table
    content</li>
  <li><b><?hilite('allow_new()');?></b> &mdash; allow insertion of new rows into
    the table</li>
  <li><b><?hilite('disable_nav()');?></b> &mdash; do not show navigation
    controls at the top</li>
  <li><b><?hilite('disable_filter()');?></b> &mdash; remove controls for
    filtering rows</li>
  <li><b><?hilite('set_n_grid_col($n)');?></b> &mdash; set the number of columns
    in the bootstrap layout being  used (default = 12)</li>
  <li><b><?hilite('set_btn_size($size)');?></b> &mdash; one of <tt>xs</tt>,
    <tt>sm</tt>, <tt>md</tt> or <tt>lg</tt> (default = <tt>sm</tt>)</li>
  <li><b><?hilite('set_table_class($css)');?></b> &mdash; for specifying extra
    classes to the <code>&gt;table&lt;</code></li>
</ul>

<p>Further control can be gained by setting some global variables of an
<code>alist</code> object.
<ul>
  <li><b>extra_join</b> &mdash; for specifying extra SQL <code>JOIN</code>
    conditions</li>
  <li><b>extra_where</b> &mdash; for specifying extra SQL <code>WHERE</code>
    conditions</li>
</ul>
<br>

<a name="col_reference"></a>
<h3>Column Constructor Arguments</h3>

<h4 class="prototype"><tt><?hilite('col($field, $heading, $width = 1, $default = null, $action = null, $css = null)');?></tt></h4>

<p>The <code>col</code> class is the base class for all the column classes
<i>and should never be used</i>: always use one of the inherited classes. But
the arguments it takes appear in all the inherited classes.</p>
<ul>
  <li><b>$field</b> &mdash; the name of the column in the database</li>
  <li><b>$heading</b> &mdash; the text to display as the title for this
  column</li>
  <li><b>$width</b> &mdash; <i>currently not used</i></li>
  <li><b>$default</b> &mdash; default value when adding rows to the table</li>
  <li><b>$action</b> &mdash; an <code>alist::action</code> object for handling
  <code>onclick</code> events; if <code>null</code> then the column will not be
  clickable</li>
  <li><b>$css</b> &mdash; for adding custom CSS code to the <code>td</code> tags
  in the column</li>
</ul>
<br>

<h4 class="prototype"><tt><?hilite('col_hidden($field, $heading, $width = 1, $default = null, $action = null, $css = null)')?></tt></h4>

<p>Queries a DB column but does not display it; you may want to use this if you
want an action for one of the columns to make use of a table value that is not
displayed.</p>
<br>

<h4 class="prototype"><tt><?hilite('col_str($field, $heading, $maxlen = -1, $width = 1, $default = null, $action = null, $css = null)');?></tt></h4>

<p>For displaying text (e.g., a <code>varchar</code> column).</p>
<ul>
  <li><b>$maxlen</b> &mdash; maximum number of characters to display before 
  inserting an ellipsis (&hellip;); if <code>-1</code> then no limit is
  imposed</li>
</ul>
<br>

<h4 class="prototype"><tt><?hilite('col_numeric($field, $heading, $width = 1, $format = "%g", $default = null, $action = null, $css = null)')?></tt></h4>

<p>For displaying numeric data.</p>
<ul>
  <li><b>$format</b> &mdash; a <code>printf</code>-style format string</li>
</ul>
<br>

<h4 class="prototype"><tt><?hilite('col_datetime($field, $heading, $format = "%Y/%m/%d_%H:%M:%S", $width = 1, $default = null, $action = null, $css = null)')?></tt></h4>

<p>For displaying dates or times.</p>
<ul>
  <li><b>$format</b> &mdash; a PHP <code>strftime</code>-style format
   string</li>
</ul>
<br>

<h4 class="prototype"><tt><?hilite('col_combo($field, $heading, $ref_table, $ref_field, $ref_pkey = "id", $ref_filt_col = null, $ref_filt_field = null, $width = 1, $default = null, $action = null, $css = null)');?></tt></h4>

<p>For displaying a column that refers to an auxiliary table via a foreign key
(normally an <code>id</code> key). Instead of displaying the foreign key, the
value from the designated column of the auxiliary table is displayed. When
editing a row or adding a new row, the options from the auxiliary table are
presented in a drop-down menu.</p>

<p>A further complexity is provided for: if there is a <i>third</i> table that
is also linked to the primary table by a foreign key that determines which
possible values can be selected in the auxiliary table, that can be specified
too. This only matters when editing or inserting rows. In the example above, 
the &lsquo;Subdepartment&rsquo; column works like this. If you edit a row, the
options available under the &lsquo;Subdepartment&rsquo; column are determined by
which value is currently selected in the &lsquo;Department&rsquo; row.</p>
this.</p>
<ul>
  <li><b>$ref_table</b> &mdash; the name of the auxiliary table</li>
  <li><b>$ref_field</b> &mdash; the field of the auxiliary table whose value
  should be displayed</li>
  <li><b>$ref_pkey</b> &mdash; the name of the foreign key in the auxiliary
  table (which is normally, but not necessarily, its primary key)</li>
  <li><b>$ref_filt_col</b> &mdash; the column in the auxiliary table that is a
  foreign key to the third table that filters the possible values of the
  auxiliary table (this and the following arguments should be left as
  <code>null</code> if the situation does not obtain)</li>
  <li><b>$ref_filt_field</b> &mdash; the column in the main table that is a
  primary key to the third table used for filtering; only rows in the auxiliary
  table that have <code>$ref_filt_col</code> values equal to the
  <code>$ref_filt_field</code> in the current row are allowed.</li>
</ul>
<br>

<h4 class="prototype"><tt><?hilite('col_enum($field, $heading, $option, $width = 1, $default = null, $action = null, $css = null)');?></tt></h4>

<p>For displaying an enum.</p>
<ul>
  <li><b>$option</b> &mdash; an associative array where the keys are possible values in the database and the values are what are displayed (e.g., <code>array("0" =&gt; "Off", "1" =&gt; "On")</code>)
</ul>
<br>

</div>

</body>

</html>

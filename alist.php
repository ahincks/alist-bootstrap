<?php

/*
ALIST - a PHP and JavaScript library for creating sortable, filterable lists
from MySQL tables. It makes use of the Bootstrap and jquery libraries.

Copyright Adam D. Hincks, S.J., 2015.

This file is part of ALIST.

ALIST is free software: you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software 
Foundation, either version 3 of the License, or (at your option) any later 
version.

ALIST is distributed in the hope that it will be useful, but WITHOUT ANY 
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with 
ALIST.  If not, see <http://www.gnu.org/licenses/>.
*/

class action {
  public $function;
  public $field;

  function __construct($function, $field) {
    $this->function = $function;
    $this->field = $field;
  }
}

class col {
  public $field;
  public $default;
  public $action;
  public $heading;
  public $width;
  public $css;

  function __construct($field, $heading, $width = 1, $default = null,
                       $action = null, $css = null) {
    $this->field = $field;
    $this->heading = $heading;
    $this->width = $width;
    $this->default = $default;
    $this->action = $action;
    $this->css = $css;
  }
}

class col_hidden extends col {
}

class col_str extends col {
  public $maxlen;

  function __construct($field, $heading, $maxlen = -1, $width = 1, 
                       $default = null, $action = null, $css = null) {
    $this->maxlen = $maxlen;
    parent::__construct($field, $heading, $width, $default, $action, $css);
  }
}

class col_numeric extends col {
  public $format;

  function __construct($field, $heading, $width = 1, $format = "%g", 
                       $default = null, $action = null, $css = null) {
    $this->format = $format;
    parent::__construct($field, $heading, $width, $default, $action, $css);
  }
}

class col_datetime extends col {
  public $format;

  function __construct($field, $heading, $format = "%Y/%m/%d&nbsp;%H:%M:%S", 
                       $width = 1, $default = null, $action = null, 
                       $css = null) {
    $this->format = $format;
    parent::__construct($field, $heading, $width, $default, $action, $css);
  }
}

class col_combo extends col {
  public $ref_table;
  public $ref_pkey;
  public $ref_field;
  public $ref_filt_col;
  public $ref_filt_field;

  function __construct($field, $heading, $ref_table, $ref_field,
                       $ref_pkey = "id", $ref_filt_col = null,
                       $ref_filt_field = null, $width = 1, $default = null,
                       $action = null, $css = null) {
    $this->ref_table = $ref_table;
    $this->ref_field = $ref_field;
    $this->ref_pkey = $ref_pkey;
    $this->ref_filt_col = $ref_filt_col;
    $this->ref_filt_field = $ref_filt_field;
    parent::__construct($field, $heading, $width, $default, $action, $css);
  }
}

class col_enum extends col {
  public $option;

  function __construct($field, $heading, $option, $width = 1, $default = null,
                       $action = null, $css = null) {
    $this->option = $option;
    parent::__construct($field, $heading, $width, $default, $action, $css);
  }
}

class alist {
  // Constants: default values.
  const DEFAULT_NULL_VAL    = "N/A";
  const DEFAULT_PERPAGE     = 25;

  // Internally-used constants.
  private static $mysql_datetime_fmt  = "%Y-%m-%d %H:%M:%S";

  // Strings.
  private $db;
  private $table;
  private $pkey;
  public $select_val;
  private $null_val;
  public $extra_join;
  public $extra_where;
  public $btn_size;
  public $table_class;

  // Boolean.
  private $allow_edit;
  private $allow_new;
  private $show_nav;
  private $allow_filter;

  // Integers.
  public $default_perpage;
  public $n_grid_col;

  // The columns.
  private $col;

  function __construct($db_in = null) {
    $this->db = $db_in;
    $this->table = null;
    $this->pkey = null;
    $this->select_val = null;
    $this->null_val = self::DEFAULT_NULL_VAL;
    $this->allow_edit = false;
    $this->allow_new = false;
    $this->show_nav = true;
    $this->allow_filter = true;
    $this->default_perpage = self::DEFAULT_PERPAGE;
    $this->extra_join = "";
    $this->extra_where = "";
    $this->n_grid_col = 12;
    $this->btn_size = "sm";
    $this->table_class = "";
    $this->col = array();
  }

  function set_table_and_pkey($table, $pkey = "id") {
    $this->table = $table;
    $this->pkey = $pkey;
  }

  function set_null_val($null_val) {
    $this->null_val = $null_val;
  }

  function set_default_perpage($n) {
    $this->default_perpage = $n;
  }

  function allow_edit() {
    $this->allow_edit = true;
  }

  function allow_new() {
    $this->allow_new = true;
  }

  function disable_nav() {
    $this->show_nav = false;
  }

  function disable_filter() {
    $this->allow_filter = false;
  }

  function add_column($column) {
    array_push($this->col, $column);
  }

  function set_n_grid_col($n) {
    $this->n_grid_col = $n;
  }

  function set_btn_size($size) {
    $this->btn_size = $size;
  }

  function set_table_class($css) {
    $this->table_class = $css;
  }

  private function qstring_unescape_str($str) {
    $str = str_replace("%25", "%", $str);
    $str = str_replace("%20", " ", $str);

    return $str;
  }

  private function sql_escape_str($str) {
    $str = str_replace("'", "\'", $str);

    return $str;
  }

  private function js_escape_str($str) {
    return str_replace("'", "&apos;", $str);
  }

  private function get_query($tag, $default = "") {
    if (!isset($_GET[$tag]))
      return $default;
    else
      $ret = $_GET[$tag];

    $ret = self::qstring_unescape_str($ret);

    return $ret;
  }

  private function make_combo_select($row, $col, $value, $combo_child,
                                     $combo_parent, $combo_data,
                                     $id_suffix = "")  {
    if ($id_suffix) {
      // If this is a filter, add the tooltip context.
      printf("<div class='input-group select2-bootstrap-prepend'>");
      printf("<span class='input-group-addon'>");
      printf("<span class='glyphicon glyphicon-question-sign' " .
             "data-toggle='tooltip' data-container='body' " .
             "style='cursor: help;' " .
             "title='Select an entry to narrow search.'></span>" .
             "</span>");
    }

    if (!$this->col[$col]->ref_filt_field) {
      // If the combo box does not depend on another, things are
      // straightforward.
      // If this combo box changes the options in another, set up the onchange
      // parameter. If it is part of a filter, make it submit the filter
      // values.
      $onchange = "";
      if (!$id_suffix) {
        foreach ($combo_child[$col] as $child) {
          if (!strlen($onchange))
            $onchange = " onchange='";
            $onchange .= sprintf("alist_show_combo(document.forms." .
                                 "alist_%s.%s%s, \"alist_%s_%s%s\");", 
                                 $this->table, $this->col[$col]->field, 
                                 $id_suffix, $this->table,
                                 $this->col[$child]->field, $id_suffix);
        }
        if (strlen($onchange))
          $onchange .= "'";
      }
      else {
        $cancel_arr = "[";
        foreach ($combo_child[$col] as $child) {
          if (strlen($cancel_arr) > 1)
            $cancel_arr .= ", ";
          $cancel_arr .= sprintf("\"%s_%s%s\"", $this->table,
                                 $this->col[$child]->field, $id_suffix);
        }
        $cancel_arr .= "]";
        $onchange = sprintf(" onchange='alist_apply_filter(document.forms." .
                            "alist_%s.%s%s, \"%s_%s%s\", %s);'",
                            $this->table, $this->col[$col]->field,
                            $id_suffix, $this->table, 
                            $this->col[$col]->field, $id_suffix,
                            $cancel_arr);
      }

      printf("<select id='%s_%s%s' name='%s%s' " .
             "class='form-control'%s>\n",
             $this->table, $this->col[$col]->field, $id_suffix, 
             $this->col[$col]->field, $id_suffix, $onchange);
      if ($id_suffix) {
        if (!$value)
          $sel = " selected";
        else
          $sel = "";
        printf("<option value=''%s>All</option>\n", $sel);
      }
      foreach ($combo_data[$col] as $opt_key => $opt_val) {
        if ($opt_key == $value)
          $sel = " selected";
        else
          $sel = "";
        printf("<option value='%s'%s>%s</option>\n", $opt_key, $sel,
               $opt_val[$this->col[$col]->ref_field]);
      }
      printf("</select>\n");
    }
    else {
      // This combo box depends on another. Make one select for each
      // of the dependencies; however, if there is an id_suffix (i.e., this is
      // for the filter), then just do the current one.
      $filt_val = $row[$this->col[$col]->ref_filt_field];
      
      if (!$id_suffix)
        printf("<div id='alist_%s_%s%s' style='position: relative;'>\n",
               $this->table, $this->col[$col]->field, $id_suffix);
      else if (!strlen($filt_val)) { 
        printf("<select id='%s_%s%s, name='%s_%s' " .
               "class='form-control'>\n",
               $this->table, $this->col[$col]->field, $id_suffix,
               $this->col[$col]->field, $id_suffix);
        printf("<option value=''>All</option>\n");
        printf("</select>\n");
        printf("</div>\n");
      }
      $first = true;
      foreach ($combo_data[$combo_parent[$col]] as $aaa => $filt_opt) {
        $curr_val = $filt_opt[$this->col[$combo_parent[$col]]->ref_pkey];
        $display = "none";
        if ($value === null) {
          if ($first) {
            $display = "block";
            $first = false;
          }
        }
        else if ($filt_val == $curr_val)
          $display = "block";
        if (!$id_suffix)
          printf("<div id='alist_%s_%s%s_%s' style='display: %s;'>\n", 
                 $this->table, $this->col[$col]->field, $id_suffix, $curr_val,
                 $display);
        if (!$id_suffix || $display == "block") {
          if ($id_suffix) {
            $onchange = sprintf("onchange='alist_apply_filter(document.forms." .
                                "alist_%s.%s_%s%s, \"%s_%s%s\", []);'", 
                                $this->table, $this->col[$col]->field,
                                $curr_val, $id_suffix, $this->table,
                                $this->col[$col]->field, $id_suffix);
          }
          else
            $onchange = "";

          printf("<select class='form-control' id='%s_%s%s_%s' " .
                 "name='%s_%s%s'%s>\n",
                 $this->table, $this->col[$col]->field, $id_suffix, $curr_val,
                 $this->col[$col]->field, $curr_val, $id_suffix, $onchange);

          if ($id_suffix) {
            if (!strlen($filt_val))
              $sel = " selected";
            else
              $sel = "";
            printf("<option value=''%s>All</option>\n", $sel);
            $none = false;
          }
          else
            $none = true;

          foreach ($combo_data[$col] as $opt_key => $opt_val) {
            if ($opt_val[$this->col[$col]->ref_filt_col] != $curr_val)
              continue;
            $none = false;
            if ($opt_key == $value)
              $sel = " selected";
            else
              $sel = "";
            printf("<option value='%s'%s>%s</option>\n", $opt_key, $sel,
                   $opt_val[$this->col[$col]->ref_field]);
          }
          if ($none)
            printf("<option value='NULL'%s>%s</option>\n", $sel,
                   $this->null_val);
          printf("</select>\n");
        }
        if (!$id_suffix)
          printf("</div>\n");
      }
      if (!$id_suffix)
        printf("</div>\n");
    }
    printf("<script>$(document).ready(function() { ".
           "$('select[id^=\"%s_%s%s\"]').select2({width: 'resolve'});});".
           "</script>", $this->table, $this->col[$col]->field, $id_suffix);
    if ($id_suffix)
      printf("</div>");
  }

  private function make_enum_select($col, $value, $id_suffix = "") {
    if ($id_suffix) {
      $onchange = sprintf(" onchange='alist_apply_filter(document.forms." .
                          "alist_%s.%s%s, \"%s_%s%s\", []);'",
                          $this->table, $this->col[$col]->field, $id_suffix,
                          $this->table, $this->col[$col]->field,
                          $id_suffix);
      printf("<div class='input-group select2-bootstrap-prepend'>");
      printf("<span class='input-group-addon'>");
      printf("<span class='glyphicon glyphicon-question-sign' " .
             "data-toggle='tooltip' data-container='body' " .
             "style='cursor: help;' " .
             "title='Select an entry to narrow search.'></span>" .
             "</span>");
    }
    else
      $onchange = "";
    printf("      <select id='%s_%s%s' name='%s%s'%s class='form-control'>\n",
           $this->table, $this->col[$col]->field, $id_suffix, 
           $this->col[$col]->field, $id_suffix, $onchange);
    if ($id_suffix) {
      if (!$value)
        $sel = "selected";
      else
        $sel = "";
      printf("        <option value=''%s>All</option>\n", $sel);
    }
    foreach ($this->col[$col]->option as $opt_key => $opt_val) {
      if ($value == $opt_key)
        $sel = " selected";
      else
        $sel = "";
      printf("        <option value='%s'%s>%s</option>\n", $opt_key, $sel,
             $opt_val);
    }
    printf("      </select>\n");
    printf("<script>$(document).ready(function() { ".
           "$('#%s_%s%s').select2({width: 'resolve'});});".
           "</script>", $this->table, $this->col[$col]->field, $id_suffix);
    if ($id_suffix)
      printf("</div>");
  }

  public function make() {
    $order_by = self::get_query($this->table . "_nav_order_by",
                                $this->pkey);
    $order_dir = self::get_query($this->table . "_nav_order_dir", "ASC");
    $edit = self::get_query($this->table . "_edit", "");
    $new = self::get_query($this->table . "_new", "");
    $nav_perpage = self::get_query($this->table . "_nav_perpage",
                                   $this->default_perpage);
    $nav_first_row = self::get_query($this->table . "_nav_first_row", 0);
    $n_filt = self::get_query($this->table . "_n_filt", 0);

    date_default_timezone_set("UTC");

    // Grab filter values, if they exist. At the same time, build the WHERE
    // string.
    $filt = array();
    if (strlen($this->extra_where)) {
      $where = " WHERE " . $this->extra_where;
      $has_extra_where = true;
    }
    else {
      $where = "";
      $has_extra_where = false;
    }
    for ($i = 0, $filt_where_added = false; $i < $n_filt; $i++) {
      $filt[$i] = array();
      for ($j = 0, $first_where = true; $j < count($this->col); $j++) {
        $fname = $this->col[$j]->field;
        if (($filt[$i][$fname] = self::get_query($this->table . "_" . $fname .
                                  "_filt_" . $i))) {
          $filt_where_added = true;
          if (!$where) {
            $where = " WHERE ((";
            $first_where = false;
          }
          else if ($has_extra_where) {
            $where .= " AND ((";
            $has_extra_where = false;
            $first_where = false;
          }
          else if ($first_where) {
            $where .= ") OR (";
            $first_where = false;
          }
          else
            $where .= " AND ";
          $where .= $fname;

          switch (get_class($this->col[$j])) {
            case "col_str":
              $where .= " LIKE '%" . $filt[$i][$fname] .
                        "%'";
              break;
            case "col_enum":
              $where .= " = '" . $filt[$i][$fname] . "'";
              break;
            case "col_combo":
              $where .= " = " . $filt[$i][$fname];
              break;
            case "col_numeric":
              $n = explode("--", $filt[$i][$fname]);
              if (count($n) > 1) {
                $where .= " BETWEEN " . trim($n[0]) . " AND " . trim($n[1]);
                $filt[$i][$fname] = trim($n[0]) . "--" . trim($n[1]);
              }
              else
                $where .= " = " . $n[0];
              break;
            case "col_datetime":
              $n = explode("--", $filt[$i][$fname]);
              if (count($n) > 1) {
                $where .= " BETWEEN '" . trim($n[0]) . "' AND '" . trim($n[1]) .
                          "'";
                $filt[$i][$fname] = trim($n[0]) . "--" . trim($n[1]);
              }
              else
                $where .= " = '" . $n[0] . "'";
              break;
          }
        }
      }
    }
    if ($filt_where_added)
      $where .= "))";
  
    // See if form data hase been posted.
    $sql = "";
    if (isset($_POST[$this->table . "_edit"])) {
      $sql = "UPDATE " . $this->table . " SET ";
      for ($i = 0, $first = true; $i < count($this->col); $i++) {
        if (get_class($this->col[$i]) == "col_hidden")
          continue;

        if (!$first)
          $sql .= ", ";
        else
          $first = false;

        $sql .= $this->col[$i]->field . " = ";
        if (get_class($this->col[$i]) == "col_combo") {
          if ($this->col[$i]->ref_filt_col)
            $val = $_POST[$this->col[$i]->field . "_" . 
                          $_POST[$this->col[$i]->ref_filt_field]];
          else
            $val = $_POST[$this->col[$i]->field];
        }
        else
          $val = $_POST[$this->col[$i]->field];

        if ($val == "")
          $sql .= "NULL";
        else {
          switch (get_class($this->col[$i])) {
            case "col_str": case "col_datetime": case "col_enum":
              $sql .= "'" . self::sql_escape_str($val) . "'";
              break;
            default:
              $sql .= self::sql_escape_str($val);
              break;
          }
        }
      }
      $sql .= " WHERE " . $this->pkey . " = " .
              $_POST[$this->table . "_edit"] . ";";
    }

    if (isset($_POST[$this->table . "_new"])) {
      $sql = "INSERT INTO " . $this->table . " (";
      for ($i = 0, $first = true; $i < count($this->col); $i++) {
        if (get_class($this->col[$i]) == "col_hidden")
          continue;
        if ($first)
          $first = false;
        else
          $sql .= ", ";
        $sql .= $this->col[$i]->field;
      }
      $sql .= ") VALUES (";
      for ($i = 0, $first = true; $i < count($this->col); $i++) {
        if (get_class($this->col[$i]) == "col_hidden")
          continue;

        if (!$first)
          $sql .= ", ";
        else
          $first = false;

        if (get_class($this->col[$i]) == "col_combo") {
          if ($this->col[$i]->ref_filt_col)
            $val = $_POST[$this->col[$i]->field . "_" . 
                          $_POST[$this->col[$i]->ref_filt_field]];
          else
            $val = $_POST[$this->col[$i]->field];
        }
        else
          $val = $_POST[$this->col[$i]->field];

        if (!$val)
          $sql .= "NULL";
        else {
          switch (get_class($this->col[$i])) {
            case "col_str": case "col_datetime": case "col_enum":
              $sql .= "'" . self::sql_escape_str($val) . "'";
              break;
            default:
              $sql .= self::sql_escape_str($val);
              break;
          }
        }
      }
      $sql .= ");";
    }

    if (strlen($sql)) {
      if ($this->db instanceof PDO) {
        $q = $this->db->prepare($sql);
        $q->execute();
      }
      else if (!$this->db->query($sql)) {
        printf("<div>\n");
        printf("Error committing changes: <i>%s</i>.\n", $this->db->error);
        printf("</div>\n");
      }
    }

    // Get the options for the COMBO types.
    $combo_data = array();
    for ($i = 0; $i < count($this->col); $i++) {
      if (get_class($this->col[$i]) != "col_combo")
        $combo_data[$i] = null;
      else {
        $sql = "SELECT " . $this->col[$i]->ref_pkey . ", " .
               $this->col[$i]->ref_field;
        if ($this->col[$i]->ref_filt_col)
          $sql .= ", " . $this->col[$i]->ref_filt_col;
        $sql .= " FROM " . $this->col[$i]->ref_table . " ORDER BY " .
                $this->col[$i]->ref_field . ";";
        $combo_data[$i] = array();
        if ($this->db instanceof PDO) {
          $q = $this->db->prepare($sql);
          $q->execute();
          while ($row = $q->fetch(PDO::FETCH_ASSOC))
            $combo_data[$i][$row[$this->col[$i]->ref_pkey]] = $row;
        }
        else {
          // MySQLi
          if (!($q = $this->db->query($sql)))
            echo $this->db->error;
          else {
            while ($row = $q->fetch_assoc())
              $combo_data[$i][$row[$this->col[$i]->ref_pkey]] = $row;
            $q->free();
          }
        }
      }
    }

    // Get any filter cross-references between COMBO types.
    $combo_child = array();
    $combo_parent = array();
    for ($i = 0; $i < count($this->col); $i++) {
      if (get_class($this->col[$i]) != "col_combo") {
        $combo_child[$i] = null;
        $combo_parent[$i] = null;
      }
      else {
        $combo_child[$i] = array();
        for ($j = 0, $n = 0; $j < count($this->col); $j++) {
          if (get_class($this->col[$j]) == "col_combo") {
            if ($this->col[$j]->ref_filt_col != null) {
              if ($this->col[$j]->ref_filt_field == $this->col[$i]->field) {
                $combo_child[$i][$n++] = $j;
                $combo_parent[$j] = $i;
              }
            }
          }
        }
      }
    }

    // Count the number of columns.
    for ($i = 0, $n_col = 0; $i < count($this->col); $i++) {
      if (get_class($this->col[$i]) != "col_hidden")
        $n_col++;
    }
    $n_col_display = $n_col;
    if ($this->allow_edit || $this->allow_new)
      $n_col_display++;

    // Ensure join statement has a space.
    if (strlen($this->extra_join)) {
      $this->extra_join = " JOIN " . $this->extra_join;
    }

    // Count the number of rows.
    $sql = "SELECT DISTINCT COUNT(*) FROM " . $this->table .
           $this->extra_join . $where . ";";
    if ($this->db instanceof PDO) {
      $q = $this->db->prepare($sql);
      $q->execute();
      $res = $q->fetch(PDO::FETCH_NUM);
    }
    else {
      $q = $this->db->query($sql);
      $res = $q->fetch_row();
    }
    $tot_n_row = $res[0];

    // Start the form.
    printf("<form id='alist_%s' method='post'>\n", $this->table);
    if ($edit)
      printf("<input type='hidden' name='%s_edit' value='%s'/>\n", $this->table,
             $edit);
    if ($new)
      printf("<input type='hidden' name='%s_new' value='1'/>\n", $this->table);

    // Make the navigation, if applicable.
    if ($this->show_nav) {
      printf("<div class='container'>");
      printf("<div class='row'>");

      // The per page input group.
      printf("<div class='col-xs-%d col-xs-offset-%d'>", 
             3 / 12 * $this->n_grid_col, 2 / 12 * $this->n_grid_col);
      printf("<div class='input-group " .
             "select2-bootstrap-prepend select2-bootstrap-append'>");
      printf("<span class='input-group-addon'>Show</span>");
      printf("<input id='%s_nav_perpage' class='form-control' " .
             "onkeypress='return alist_check_enter(event, alist_nav_perpage, " .
             "\"%s\");' onchange='alist_nav_perpage(\"%s\");' " .
             "value='%s'>\n", $this->table,
             $this->table, $this->table, $nav_perpage);
      printf("<span class='input-group-addon'>per page</span>");
      printf("</div></div>\n");

      // The navigation input group.
      printf("<div class='col-xs-%d'>" .
             "<div class='input-group select2-bootstrap-prepend " .
             "select2-bootstrap-append'>", 3 / 12 * $this->n_grid_col);

      // Left chevron.
      printf("<div class='input-group-btn'>");
      if ($nav_first_row - $nav_perpage < 0)
        printf("<button class='btn btn-default disabled' type='button'>");
      else
        printf("<button class='btn btn-default' type='button' " .
               "onclick='alist_go_page(\"%s\", %d);'>", 
               $this->table, $nav_first_row - $nav_perpage);
      printf("<span class='glyphicon glyphicon-chevron-left'></span>&nbsp;");
      printf("</button></div>");

      // Dropdown.
      printf("<select id='%s_nav_dropdown' class='form-control' ".
             "onchange='alist_go_page(\"%s\", " .
             "this.options[this.selectedIndex].value);'>\n", $this->table,
             $this->table);
      for ($i = 0; $i < $tot_n_row; $i += $nav_perpage) {
        if ($i >= $nav_first_row && $i < $nav_first_row + $nav_perpage)
          $sel = " selected";
        else
          $sel = "";
        printf("<option value='%d'%s>%d to %d</option>\n", $i, $sel, $i + 1,
               min($tot_n_row, $i + $nav_perpage));
      }
      printf(" </select>\n");
      printf("<script>$(document).ready(function() { ".
             "$('#%s_nav_dropdown').select2(" .
             "{minimumResultsForSearch: -1});});</script>", $this->table);

      // Right chevron.
      printf("<span class='input-group-addon'>of %d</span>", $tot_n_row); 
      printf("<div class='input-group-btn'>");
      if ($nav_first_row + $nav_perpage >= $tot_n_row)
        printf("<button class='btn btn-default disabled' type='button'>");
      else
        printf("<button class='btn btn-default' type='button' " .
               "onclick='alist_go_page(\"%s\", %d);'>", 
               $this->table, $nav_first_row + $nav_perpage);
      printf("&nbsp;<span class='glyphicon glyphicon-chevron-right'></span>");
      printf("</button></div></div></div>");

      // Filter.
      if ($this->allow_filter) {
        printf("<div class='col-xs-%d'><span>", 2 / 12 * $this->n_grid_col);
        printf("<button class='btn btn-default' type='button' " .
               "onclick='alist_n_filter(\"%s\", %d);'>", 
               $this->table, $n_filt + 1);
        printf("<span class='glyphicon glyphicon-plus-sign'></span>&nbsp;");
        printf("<span class='glyphicon glyphicon-filter'></span>");
        printf("</button>&nbsp;");
        printf("<button class='btn btn-default' type='button' " .
               "onclick='alist_n_filter(\"%s\", %d);'%s>", 
               $this->table, $n_filt - 1, $n_filt ? "" : " disabled");
        printf("<span class='glyphicon glyphicon-minus-sign'></span>&nbsp;");
        printf("<span class='glyphicon glyphicon-filter'></span>");
        printf("</button>");
        printf("</span></div>");
      }
      printf("</div></div>");
    }

    // Build query.
    $sql = "SELECT DISTINCT ";
    $pkey_used = false;
    for ($i = 0; $i < count($this->col); $i++) {
      if ($i)
        $sql .= ", ";
      $sql .= $this->col[$i]->field;
      if ($this->col[$i]->field == $this->pkey)
        $pkey_used = false;
    }
    if (!$pkey_used) {
      if ($i)
        $sql .= ", ";
      $sql .= $this->pkey;
    }
    $sql .= " FROM " . $this->table;
    $sql .= $this->extra_join;
    $sql .= $where;
    if (strlen($order_by))
      $sql .= " ORDER BY " . $order_by . " " . $order_dir;
    if ($this->show_nav)
      $sql .= " LIMIT " . $nav_first_row . ", " . $nav_perpage;
    $sql .= ";";

    // Run query.
    if ($this->db instanceof PDO) {
      $q = $this->db->prepare($sql);
      $q->execute();
    }
    else {
      if (!($q = $this->db->query($sql)))
        echo $this->db->error;
    }

    printf("<br>");
    printf("<table class='table table-striped table-hover%s'>\n",
           strlen($this->table_class) ? " " . $this->table_class : "");

    // Make the header.
    printf("<thead>");
    printf("<tr id='%s_tr_header'>\n", $this->table);
    for ($i = 0; $i < count($this->col); $i++) {
      if (get_class($this->col[$i]) == "col_hidden")
        continue;
      printf("<th style='vertical-align: baseline;'>\n");
      printf("<a href='#' " .
             "onclick='alist_order(\"%s\", \"%s\");'>", $this->table, 
             $this->col[$i]->field);
      printf("%s</a>&nbsp;&nbsp;", $this->col[$i]->heading);
      if ($this->col[$i]->field == $order_by) {
        if ($order_dir == "ASC")
          printf("<span class='glyphicon glyphicon-chevron-up'></span>");
        else
          printf("<span class='glyphicon glyphicon-chevron-down'></span>");
      }
      printf("</th>\n");
    }
    if ($this->allow_edit || $this->allow_new) {
      printf("<th style='text-align: center;'>");
      if ($this->allow_new) {
        printf("<button class='btn btn-primary btn-%s' type='button' " .
               "data-toggle='tooltip' title='Add new row.' " .
               "onclick='alist_new(\"%s\");'>", $this->btn_size, $this->table);
        printf("<span class='glyphicon glyphicon-plus-sign'></span></button>");
      }
      else
        printf("&nbsp;");
      printf("</th>\n");
    }
    printf("</tr>\n");

    // Make the filters.
    for ($i = 0; $i < $n_filt; $i++) {
      printf("<tr>\n");
      for ($j = 0; $j < count($this->col); $j++) {
        if (get_class($this->col[$j]) == "col_hidden")
          continue;
        $value = $filt[$i][$this->col[$j]->field];
        $jscript_args = sprintf("document.forms.alist_%s.%s_filt_%d, " .
                                "\"%s_%s_filt_%d\", []", $this->table,
                                $this->col[$j]->field, $i, $this->table,
                                $this->col[$j]->field, $i);

        printf("<td style='border-top: 0;'>\n");
        switch (get_class($this->col[$j])) {
          case "col_combo":
            self::make_combo_select($filt[$i], $j, $value, $combo_child,
                                    $combo_parent, $combo_data, 
                                    "_filt_" . $i);
            break;
          case "col_enum":
            self::make_enum_select($j, $value, "_filt_" . $i);
            break;
          case "col_datetime":
            printf("<div class='input-group'>");
            printf("<span class='input-group-addon'>");
            printf("<span class='glyphicon glyphicon-question-sign' " .
                   "data-toggle='tooltip' data-container='body' " .
                   "style='cursor: help;' " .
                   "title='The date format is \"Y-m-d H:M:S\", where the " .
                   "time of day (\"H:M:S\") is optional. A single date " .
                   "searches for an exact match; \"Y-m-d H:M:S -- " .
                   "Y-m-d H:M:S\" searches for a range of dates (note the " .
                   "double hypen (\"--\").'></span></span>");
            printf("<input name='%s_filt_%d' class='form-control' value='%s' " .
                   "onkeypress='return alist_check_enter(event, " .
                   "alist_apply_filter, %s);' " .
                   "onchange='alist_apply_filter(%s);'/></div>", 
                   $this->col[$j]->field, $i, self::js_escape_str($value),
                   $jscript_args, $jscript_args);
            break;
          case "col_numeric":
            printf("<div class='input-group'>");
            printf("<span class='input-group-addon'>");
            printf("<span class='glyphicon glyphicon-question-sign' " .
                   "data-toggle='tooltip' data-container='body' " .
                   "style='cursor: help;' " .
                   "title='\"1.23\" searches for an exact match; " .
                   "\"1.23--2.23\" searches for a range (note the double " .
                   "hyphen, which is required).'></span></span>");
            printf("<input name='%s_filt_%d' class='form-control' value='%s' " .
                   "onkeypress='return alist_check_enter(event, " .
                   "alist_apply_filter, %s);' " .
                   "onchange='alist_apply_filter(%s);'/></div>", 
                   $this->col[$j]->field, $i, self::js_escape_str($value),
                   $jscript_args, $jscript_args);
            break;
          case "col_str":
            printf("<div class='input-group'>");
            printf("<span class='input-group-addon'>");
            printf("<span class='glyphicon glyphicon-question-sign' " .
                   "data-toggle='tooltip' data-container='body' " .
                   "style='cursor: help;' " .
                   "title='Search for anything containing the given " .
                   "string.'></span></span>");
            printf("<input name='%s_filt_%d' class='form-control' value='%s' " .
                   "onkeypress='return alist_check_enter(event, " .
                   "alist_apply_filter, %s);' " .
                   "onchange='alist_apply_filter(%s);'/></div>", 
                   $this->col[$j]->field, $i, self::js_escape_str($value), 
                   $jscript_args, $jscript_args);
            break;
        }
        printf("</td>\n");
      }
      if ($this->allow_edit || $this->allow_new)
        printf("<td style='border-top: 0'>&nbsp;</td>\n");
      printf("</tr>\n");
    }
    printf("</thead><tbody>");

    if ($this->db instanceof PDO)
      $n_row = $q->rowCount();
    else
      $n_row = $q->num_rows;

    if (!$n_row)
      printf("<tr><td colspan='%d'>" .
             "<center><i>No records</i></center></td></tr>\n", $n_col_display);

    // Fill in the rows.
    for ($i = 0; $i < $n_row; $i++) {
      if ($new) {
        for ($j = 0; $j < count($this->col); $j++) {
          if ($this->col[$j]->default)
            $row[$this->col[$j]->field] = $this->col[$j]->default;
          else
            $row[$this->col[$j]->field] = null;
        }
        if (!$pkey_used)
          $row[$this->pkey] = null;
        $new = false;
        $i--;
      }
      else {
        if ($this->db instanceof PDO)
          $row = $q->fetch(PDO::FETCH_ASSOC);
        else
          $row = $q->fetch_assoc();
      }
        
      if ($this->allow_edit && $row[$this->pkey] == $edit)
        $editing = true;
      else
        $editing = false;

      if ($editing)
        printf("  <tr>\n");
      else if ($this->pkey && $row[$this->pkey] == $this->select_val)
        printf("  <tr>\n");

      for ($j = 0; $j < count($this->col); $j++) {
        if (get_class($this->col[$j]) == "col_hidden")
          continue;
        $value = $row[$this->col[$j]->field];
          
        if (isset($this->col[$j]->css))
          printf("<td style='%s'>\n", $this->col[$j]->css);
        else
          printf("<td>\n");

        if (!$editing) {
          if ($this->col[$j]->action) {
          printf("<a href='#' onclick='%s(\"%s\");'>",
                 $this->col[$j]->action->function, 
                 $row[$this->col[$j]->action->field]);
          }
          else
            printf("      ");
        }
  
        switch (get_class($this->col[$j])) {
          case "col_combo":
            if ($this->allow_edit && $row[$this->pkey] == $edit)
              self::make_combo_select($row, $j, $value, $combo_child,
                                      $combo_parent, $combo_data);
            else {
              if ($value)
                printf("%s",
                       $combo_data[$j][$value][$this->col[$j]->ref_field]);
              else
                printf("%s", $this->null_val);
            }
            break;

          case "col_enum":
            if ($this->allow_edit && $row[$this->pkey] == $edit)
              self::make_enum_select($j, $value);
            else
              printf("%s", $this->col[$j]->option[$value]);
            break;

          case "col_datetime":
            if ($this->allow_edit && $row[$this->pkey] == $edit)
              printf("<input class='form-control' name='%s' value='%s'/>\n",
                     $this->col[$j]->field, self::js_escape_str($value));
            else {
              $d = strptime($value, self::$mysql_datetime_fmt);
              $d = mktime($d["tm_hour"], $d["tm_min"], $d["tm_sec"],
                          $d["tm_mon"], $d["tm_mday"], $d["tm_year"] + 1900);
              printf("%s", strftime($this->col[$j]->format, $d));
            }
            break;

          case "col_numeric":
            if ($this->allow_edit && $row[$this->pkey] == $edit)
              printf("<input class='form-control' name='%s' value='%s'/>\n",
                     $this->col[$j]->field, $value);
            else {
              if ($value !== null)
                printf($this->col[$j]->format, $value);
              else
                printf("%s", $this->null_val);
            }
            break;

          case "col_str":
            if ($this->allow_edit && $row[$this->pkey] == $edit) {
              printf("<input class='form-control' name='%s' value='%s'/>\n",
                     $this->col[$j]->field, self::js_escape_str($value));
            }
            else {
              if ($this->col[$j]->maxlen > 0) {
                if (strlen($value) > $this->col[$j]->maxlen)
                  $value = substr($value, 0, $this->col[$j]->maxlen) .
                           " &hellip;";
              }
              if ($value === null)
                printf("%s", $this->null_val);
              else if (!strlen($value))
                $value = "&nbsp;";
              else
                printf("%s", $value);
            }
            break;

          default:
            printf("%s", $value);
        }
        if (!$editing && $this->col[$j]->action)
          printf("</a>\n");
        else
          printf("\n");
        printf("    </td>\n");
      }
      if ($this->allow_edit || $this->allow_new) {
        printf("<td style='white-space: nowrap; text-align: center;'>\n");
        if ($this->allow_edit) {
          if ($this->allow_edit && $row[$this->pkey] == $edit) {
            printf("<button class='btn btn-success btn-%s' type='button' " .
                   "onclick='alist_submit(\"%s\");'>" .
                   "<span class='glyphicon glyphicon-ok'></span></button> ",
                   $this->btn_size, $this->table);
            printf("<button class='btn btn-warning btn-%s' type='button' " .
                   "onclick='alist_cancel(\"%s\");'>" .
                   "<span class='glyphicon glyphicon-remove'></span>" .
                   "</button>", $this->btn_size, $this->table);
          }
          else {
            printf("<button class='btn btn-regular btn-%s' type='button' " .
                   "onclick='alist_edit(\"%s\", \"%s\");'>" .
                   "<span class='glyphicon glyphicon-edit'></span></button> ",
                   $this->btn_size, $this->table, $row[$this->pkey]);
          }
        }
        
        printf("</td>\n");
      }
      printf("</tr>\n");
    }
    printf("</tbody>\n");
    printf("</table>\n");
    printf("</form>\n");

    printf("<script>$(document).ready(function() {" .
           "$('[data-toggle=\"tooltip\"]').tooltip({placement: 'bottom'});});</script>");

    return;
  }

};
?>
